var express 			= require('express');
var util 				= require('util');
var session 			= require('express-session');
var cookieParser 		= require('cookie-parser');
var bodyParser 			= require('body-parser');
var config 				= require('./configuration/config');
var mysql 				= require('mysql');
var bodyParser 			= require("body-parser");
var morgan    			= require("morgan");
var passport 			= require('passport'); 
var LocalStrategy 		= require('passport-local').Strategy;
var FacebookStrategy 	= require('passport-facebook').Strategy;
var http 				= require('http');
var path 				= require('path');

var app = express();

// Define MySQL parameter in Config.js file.
var connection = mysql.createConnection({
	host : config.host,
	user : config.username,
	password : config.password,
	database : config.database
});
// Connect to Database .
if (config.use_database === 'true') {
	connection.connect();
	console.log(" I'm connected to Database ;) ");
}
var accessmode ;
// Passport session setup.https://vickev.com/#!/article/authentication-in-single-page-applications-node-js-passportjs-angularjs
//=========================teeeeeeeest=========================================
//Define the strategy to be used by PassportJS
passport.use(new LocalStrategy(
function(username, password, done) {
// if ((username === "admin" && password === "admin") ||(username === "superadmin" && password === "superadmin") || (username === "user" && password === "user")) // stupid example
//	if(true)
	 

	if(config.use_database==='true')
	{
	connection.query("SELECT * from tb_users where username='" +username+ "'AND password='"+password+"'",function(err,rows,fields){
	
//	  connection.query("SELECT * from user_info",function(err,rows,fields){
	  if(err) throw err;
	  if(rows.length===0)
	    {
		  return done(null, false, { message: 'Incorrect username.' });
		  
	    }
	    else
	      {
	    	accessmode = rows[0].id_profile;
	    	return done(null, {name: "admin"});
	    	
	      }
	    });
	}
	
	
}
));

//Serialized and deserialized methods when got from session
passport.serializeUser(function(user, done) {
 done(null, user);
});

passport.deserializeUser(function(user, done) {
 done(null, user);
});

//Define a middleware function to be used for every secured routes
var auth = function(req, res, next){
if (!req.isAuthenticated()) 
	{res.send(401);
	res.redirect('/');
	}

else{next();}
	
};
//==================================================================
//all environments
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser()); 
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session({ secret: 'securedsession' }));
app.use(passport.initialize()); // Add passport initialization
app.use(passport.session());    // Add passport initialization
//app.use(app.router);
//app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//==================================================================













// ROUTES ======================== attention
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/app');
app.use(express.static(__dirname + '/app'));
app.use(app.router);
console.log(__dirname);

app.get('/', function(req, res) {
	res.render('index.html');

});

//Begin test+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	//==================================================================
	app.get('/users', auth, function(req, res){
	  res.send([{name: "user1"}, {name: "user2"}]);
	});
	//==================================================================

	//==================================================================
	// route to test if the user is logged in or not
	app.get('/loggedin', function(req, res) {
	  res.send(req.isAuthenticated() ? req.user : '0');
	});

	// route to log in
	app.post('/login', passport.authenticate('local'), function(req, res) {
	  res.send(req.user);
	});

	// route to log out
	app.post('/logout', function(req, res){
	  req.logOut();
	  res.send(200);
	});
//End test++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


app.get('/data.json',   function(req, res, next) {
	res.json({
		'title' : 'Data',
		'mydata' : 'docs',
		'isAdmin' : accessmode
	});
	console.log(res.data);
});

app.get('/resume', auth, function(req, res) {
	res.render('index.html');
});

app.get('/candidate', auth, function(req, res) {
	res.render('index.html');
});

app.get('/candidate2', auth, function(req, res) {
	res.render('resume-edit/resume-edit.template.html', {
		user : "4041451515151515todo"
	});
});

app.get('/candidate/:candidateId', auth, function(req, res) {
	res.render('index.html');
});

app.get('/allcandidates', auth, function(req, res) {
	res.render('index.html');
});

app.get('/addAdministrator', auth, function(req, res) {
	res.render('index.html');
});

app.get('*', function(req, res) {
	res.render('index.html');
});

app.listen(3003);
console.log("connect to localhost:3003");