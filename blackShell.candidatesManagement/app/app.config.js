'use strict';

angular
		.module('candidatesmanagement')
		.config(
				[
						'$locationProvider',
						'$routeProvider',
						'$httpProvider',

						function config($locationProvider, $routeProvider,
								$httpProvider) {

							var $cookies;
							angular.injector([ 'ngCookies' ]).invoke(
									[ '$cookies', function(_$cookies_) {
										$cookies = _$cookies_;
									} ]);
							  //================================================
						    // Check if the user is connected
						    //================================================
						    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope){
						      // Initialize a new promise
						      var deferred = $q.defer();

						      // Make an AJAX call to check if the user is logged in
						      $http.get('/loggedin').success(function(user){
						        // Authenticated
						        if (user !== '0')
						          /*$timeout(deferred.resolve, 0);*/
						          deferred.resolve();

						        // Not Authenticated
						        else {
						          $rootScope.message = 'You need to log in.';
						          //$timeout(function(){deferred.reject();}, 0);
						          deferred.reject();
						          $location.url('/');
						        }
						      });

						      return deferred.promise;
						    };
						    //================================================
						    
						    //================================================
						    // Add an interceptor for AJAX errors
						    //================================================
						    $httpProvider.interceptors.push(function($q, $location) {
						      return {
						        response: function(response) {
						          // do something on success
						          return response;
						        },
						        responseError: function(response) {
						          if (response.status === 401)
						            $location.url('/login');
						          return $q.reject(response);
						        }
						      };
						    });
						    //================================================

							// $locationProvider.hashPrefix('BlackShell');
							$routeProvider
									.when(
											'/',
											{
												template : '<black-shell-home></black-shell-home>'
											})
											
//									.when('/admin', {
//										        templateUrl: 'views/admin.html',
//										        controller: 'AdminCtrl',
//										        resolve: {
//										          loggedin: checkLoggedin
//										        }
//										      })
//							        .when('/login', {
//										        templateUrl: 'views/login.html',
//										        controller: 'LoginCtrl'
//										      })
									.when(
											'/candidate',
											{
												resolve : {
													loggedin: checkLoggedin
//													"check" : function(	$location) {
//														if ($cookies
//																.get('signedin') != "true") {
//															// todo cheeck if is
//															// * connected
//															$location
//																	.path('/home');
//														}
//													}

											          
												},
												template : '<resume-candidate-edit></resume-candidate-edit>'
											})
									.when(
											'/resume',

											{
												resolve : {  
													loggedin: checkLoggedin
//													"check" : function(	$location) {
//														if ($cookies
//																.get('signedin') != "true") {
//															// todo cheeck if is
//															// * connected
//															$location
//																	.path('/home');
//														}

												         
//													}
													
												},
												template : '<resume-candidate-load></resume-candidate-load>'
											})
									.when(
											'/allcandidates',
											{
												resolve : {
													loggedin: checkLoggedin
//													"check" : function(	$location) {
//														if ($cookies
//																.get('signedin') != "true") {
//															// todo cheeck if is
//															// * connected
//															$location
//																	.path('/home');
//														}
//													}

											          
												},
												template : '<resume-candidate-list></resume-candidate-list>'
											})
									.when(
											'/candidate/:candidateId',

											{
												resolve : {
													loggedin: checkLoggedin
//													"check" : function( $location) {
//														if ($cookies
//																.get('signedin') != "true") {
//															// todo cheeck if is
//															// * connected
//															$location
//																	.path('/home');
//														}
//													}
												},
												template : '<resume-candidate-list></resume-candidate-list>'
											})
									.when(
											'/addAdministrator',

											{
												resolve : {floggedin: checkLoggedin
//													"check" : function(	$location) {
//														if ($cookies.get('signedin') != "true") {
//															// todo cheeck if is
//															// * connected
//															$location
//																	.path('/home');
//														}
//													}
												},
												template : '<add-administrator></add-administratort>'
											})

									.otherwise({
										redirectTo : '/'
									});
							$locationProvider.html5Mode(true);
							$locationProvider.hashPrefix('!');

						} ]).run(function($rootScope, $cookies, $http){
						    $rootScope.message = '';

						    // Logout function is available in any pages
						      $rootScope.logout = function(){
						      $cookies.put('useracess', "0");
//						      $rootScope.message = 'Logged out.';
						      $http.post('/logout');
						    };
						  })

		.controller(
				'HomeCtrl',
				function($scope, $cookies, $location, $http) {
					$scope.test1 = true;
					$scope.test2 = false;

					$scope.loggedout = function loggedout() {
						// $cookies.put('notConnect', true);s
						$cookies.put('useracess', "0");
						$cookies.put('signedin', "false");
						$location.path('/');
					};
//					 $scope.getIsAdmin = 0 ;
//					$http.get('data.json').success(function(data){
//					      $scope.articles= data.isAdmin ;
//					      
//					    });
//					
//					$cookies.put('useracess', data.isAdmin); 
					
					$scope.useraccess = function useraccess(x) {
						if (x == 1) {
							return ($cookies.get('useracess') == "1");
						} else {
							if (x == 2) {
								return ($cookies.get('useracess') == "2");
							} else {
								if (x == 3) {
									return ($cookies.get('useracess') == "3");
								} else {
									if ((x == 0)
											&& (!($cookies.get('useracess') == "1"))
											&& (!($cookies.get('useracess') == "2"))
											&& (!($cookies.get('useracess') == "3"))) {
										return true;
									}

								}
							}
						}
					};

					// $scope.isaccessUser = function isaccessUser() {
					// };
					// $scope.isaccessAdmin = function isaccessAdmin() {
					// };
					// $scope.isaccessSuperAdmin = function isaccessSuperAdmin()
					// {
					// return ($cookies.get('accessSuperAdmin') == "false");
					// };
					// $scope.notconnect = $cookies.get('notConnect');
					// $scope.accessUser = $cookies.get('accessUser');
					// $scope.accessAdmin = $cookies.get('accessAdmin');
					// $scope.accessSuperAdmin =
					// $cookies.get('accessSuperAdmin');

					/* sign out function */
					self.loggedout = function loggedout() {
						$cookies.put('signedin', "false");
						$cookies.put('notConnect', true);
						$location.path('/home');
					};

				})



/**********************************************************************
 * Login controller
 **********************************************************************/
.controller('LoginCtrl', function($scope, $rootScope, $http, $location) {
  // This object will be filled by the form
  $scope.user = {};

  // Register the login() function
  $scope.login = function(){
    $http.post('/login', {
      username: $scope.user.username,
      password: $scope.user.password,
    })
    .success(function(user){
      // No error: authentication OK
      $rootScope.message = 'Authentication successful!';
      $location.url('/candidature');
    })
    .error(function(){
      // Error: authentication failed
      $rootScope.message = 'Authentication failed.';
      $location.url('/login');
    });
  };
})



/**********************************************************************
 * Admin controller
 **********************************************************************/
.controller('AdminCtrl', function($scope, $http) {
  // List of users got from the server
  $scope.users = [];

  // Fill the array to display it in the page
  $http.get('/users').success(function(users){
    for (var i in users)
      $scope.users.push(users[i]);
  });
});
