angular
		.module('resumeEdit')
		.component(
				'resumeCandidateEdit',
				{
					templateUrl : 'resume-edit/resume-edit.template.html',
					controller : [

							'candidateLoad',
							'candidateLoadNew',
							'$scope',
							'$cookies',
							'$location',
							function candidateListController(candidateLoad,
									candidateLoadNew, $scope, $cookies,
									$location) {
								$scope.myCookieVal = $cookies.get('cookie');

								var self = this;
								self.IsCreated = false;

								/* load the resume from the json file */
								self.loadCandidate = function loadCandidate() {
									self.candidate = candidateLoad.query();
									self.setShow(0);
									$scope.setShowRightPanel(1);
									self.IsCreated = true;
								};
								/* funcion used to create a new resume */
								self.addNewResume = function addNewResume() {
									self.candidate = candidateLoadNew.query();
									self.setShow(0);
									$scope.setShowRightPanel(3);
									self.IsCreated = true;
								};
								/*
								 * function used by bottons to adding new
								 * projects, educations, skills, interests or
								 * tasks
								 */
								self.addNewParameters = function addNewParameters() {
									arguments[0].length = arguments[0].length + 1;
								};
								/*
								 * funcion used to hide and show the differents
								 * parts of the webpage
								 */
								$scope.tabShow = [ true, false, false, false,
										false ];
								self.setShow = function setShow(x) {
									for (var i = 0; i < $scope.tabShow.length; i++) {
										$scope.tabShow[i] = false;
									}
									$scope.tabShow[x] = true;
								};
								/*
								 * funcion used to hide and show the right part
								 * of the webpage
								 */
								$scope.tabShowRight = [ true, false, false,
										false ];
								$scope.setShowRightPanel = function setShowRightPanel(
										x) {
									for (var i = 0; i < $scope.tabShowRight.length; i++) {
										$scope.tabShowRight[i] = false;
									}
									$scope.tabShowRight[x] = true;

								};
								/* sign out function*/
								self.loggedout = function loggedout() {
									$cookies.put('signedin', "false");
									$location.path('/');
								};
								/* edit preview show */
								$scope.editpreviewtabShow = false;
								
								self.setEditpreviewtabShow = function setEditpreviewtabShow(y) {
									$scope.editpreviewtabShow = y;
								};
								
								
								
								self.candidate = candidateLoad.query();
								self.setShow(0);
								self.IsCreated = true;
								$scope.setShowRightPanel(1);
								

							} ]
				});
