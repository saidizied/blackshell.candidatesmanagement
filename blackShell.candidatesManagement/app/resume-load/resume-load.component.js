angular.module('resumeLoad').component(
		'resumeCandidateLoad',
		{
			templateUrl : 'resume-load/resume-load.template.html',
			controller : [
					'candidateLoad',
					'$routeParams',
					'$scope',
					'$cookies','$location',
					function resumeCandidateShow(candidateLoad, $routeParams,
							$scope, $cookies, $location) {
						var self = this;
						$scope.myCookieVal = $cookies.get('cookie');
//						$cookies.put('cookie', 'vaeeffffeel');
//						
						self.setCookie = function setCookie(val) {
							$cookies.put('cookie', val);
						}
						
						
						this.resumeId = $routeParams.resumeId;
						self.candidate = candidateLoad.query();
						$scope.tabShow = [ true, false, false, false, false ];
						self.setShow = function setShow(x) {
							for (var i = 0; i < $scope.tabShow.length; i++) {
								$scope.tabShow[i] = false;
							}
							$scope.tabShow[x] = true;
						};
						/* sign out function*/
						self.loggedout = function loggedout() {
							$cookies.put('signedin', "false");
							$location.path('/');
						};
					} ]
		});