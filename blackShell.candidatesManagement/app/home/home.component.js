angular
		.module('home')
		.component(
				'blackShellHome',
				{
					templateUrl : 'home/home.template.html',
					controller : [
							'$location',
							'$scope',
							'$rootScope',
							'$cookies',
							'$http',
							function candidateListController($location,  $scope, $rootScope, $cookies, $http) {

								var self = this;
								$scope.user = {};
								$scope.accessAdmin ;
								
//								$scope.getIsAdmin = 0 ;
								
								$http.get('data.json').success(function(data){
								    $scope.getIsAdmin= data.isAdmin ;
								});
//								$scope.accessAdmin = $scope.getIsAdmin ;
								
								self.login = function login() {
									
														
									/*
									 * used to show error if email or password
									 * are empty
									 */
									if ($scope.email == "" || $scope.email == null) {
										self.addemail = true;
										self.incorrectident = false;
									} else {
										self.addemail = false;
										if ($scope.password == "" || $scope.password == null) 
										{
											self.addpassword = true;
											self.incorrectident = false;
										} else {
											self.addpassword = false;
										}
									}
									/*
									 * used to show error if email or password
									 * are incorrect
									 */
									if ((!($scope.email == "" || $scope.email == null) && !($scope.password == "" || $scope.password == null))) {
										self.incorrectident = true;
									}
									// begin main login()================================================== 
									 $scope.user.username = $scope.email ; 
									 $scope.user.password = $scope.password ;
									 $http.post('/login', {
										  username: $scope.user.username,
									      password: $scope.user.password,
									    })
									    .success(function(user){
									      // No error: authentication OK
									      $rootScope.message = 'Authentication successful!';
									      $location.url('/resume');
									    })
									    .error(function(){
									      // Error: authentication failed
									      $rootScope.message = 'Authentication failed.';
									      $location.url('/');
									    });
									 
////									 	$scope.getIsAdmin = 0 ;
//										$http.get('data.json').success(function(data){
//										      $scope.articles= data.isAdmin ;
//										      $scope.getIsAdmin= data.isAdmin ;
//										    });
//										
//										$cookies.put('useracess',  $scope.getIsAdmin); 
									 
									 
									if ($scope.email == "user"
										&& $scope.password == "user") {
										$cookies.put('signedin', "true");
										$cookies.put('useracess', $scope.getIsAdmin);
//										$location.path('/resume');

									}
									if ($scope.email == "admin"
										&& $scope.password == "admin") {
										$cookies.put('signedin', "true");
										$cookies.put('useracess', $scope.getIsAdmin);
//										$location.path('/resume');
									}
									if ($scope.email == "superadmin"
										&& $scope.password == "superadmin") {
										$cookies.put('signedin', "true");
										$cookies.put('useracess', $scope.getIsAdmin);
//										$location.path('/resume');
									}
								};
								$scope.useraccess = function useraccess(x) {
									if (x == 1) {
										return ($cookies.get('useracess') == "1");
									} else {
										if (x == 2) {
											return ($cookies.get('useracess') == "2");
										} else {
											if (x == 3) {
												return ($cookies
														.get('useracess') == "3");
											} else {
												if ((x == 0)
														&& (!($cookies
																.get('useracess') == "1"))
														&& (!($cookies
																.get('useracess') == "2"))
														&& (!($cookies
																.get('useracess') == "3"))) {
													return true;
												}

											}
										}
									}
								};
								
								$scope.register = function register() {

									// todo

								};
							} ]
				});
