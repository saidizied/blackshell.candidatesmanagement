'use strict';
// Define the `core.candidate` service
angular.module('core.candidate').factory('candidateLoadNew',
		[ '$resource', function($resource) {
			return $resource('candidates-data/:candidateID.json', {}, {
				query : {
					method : 'GET',
					params : {
						candidateID : 'candidateNew'
					},
					isArray : false
				}
			});
		} ]).factory('candidateLoad', [ '$resource', function($resource) {
	return $resource('candidates-data/:candidateID.json', {}, {
		query : {
			method : 'GET',
			params : {
				candidateID : 'candidate'
			},
			isArray : false
		}
	});
} ]).factory('serviceresumelist', ['$resource', function($resource) {
      return $resource('candidates-data/:resume.json', {}, {
        query: {
          method: 'GET',
          params: {resume:'allcandidate'
        },
          isArray: true
        }
      });
    }
  ])
  ;



