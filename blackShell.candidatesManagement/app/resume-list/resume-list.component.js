angular.module('resumeList').component('resumeCandidateList', {
	templateUrl : 'resume-list/resume-list.template.html',
	controller : [ 'serviceresumelist',
	function candidateListController(serviceresumelist) {
		
		
		 this.candidates = serviceresumelist.query();
	     this.orderProp = 'age';
	     
	     
	} ]
});
