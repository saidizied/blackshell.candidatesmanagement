module.exports = {
	/*
	 * This file contains the configurations information of Twitter login app.
	 * It consists of Twitter app information, database information.
	 */

	"facebook_api_key" 		: "todo",
	"facebook_api_secret" 	: "todo",
	"callback_url" 			: "http://localhost:3003/auth/facebook/callback",
	"use_database" 			: "true",
	"host" 					: "localhost",
	"username" 				: "root",
	"password" 				: "",
	"database" 				: "monteiz_blackshell"
							  
}
